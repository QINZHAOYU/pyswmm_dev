# -*- encoding: utf-8 -*-
# -------------------------------------------------------------
''' 
@Copyright :  Copyright(C) 2020, KeepSoft. All rights reserved.  
@License   :  LGPL-3.0  

@File      :  SluicePumpOperation.py  
@Time      :  2020/09/18  
@Author    :  Qin ZhaoYu   
@Version   :  1.0    

@Desc      :  To call swmm51013 dll to run swmm operation simulation.   
'''
# -------------------------------------------------------------


from enum import Enum, unique
from ctypes import *
from abc import ABCMeta, abstractmethod
from sko.PSO import PSO
from pyswmm_dev.utils.Logger import MyLogger
from pyswmm_dev.utils.FileHandler import MyStringHandler as strTool
from pyswmm_dev.utils.FileHandler import MyFileHandler as fileTool
from pyswmm_dev.utils.TimeHandler import MyTimeHandler as timeTool


# set up logger.
if __name__ == "pyswmm_dev.core.SluicePumpOperation":
    import logging
    logger = logging.getLogger("myLogger")
else:
    log = MyLogger("myLogger")
    log.setLogLevel(MyLogger.DEBUG)
    log.addFileHandler(logfile="./", level=MyLogger.DEBUG)
    logger = log.getLogger()


@unique
class SimMode(Enum):
    '''Operation simulation mode. 
    '''
    SIM_SWMM = 1  # run swmm without dispatch.
    SIM_PUMP = 2  # run pumps dispatching simulations only.
    SIM_SLUICE = 3  # run sluices dispatching simualtions only.
    SIM_COMBINE = 4  # run pumps and sluices dispatching simulations.


@unique
class TargetFuncType(Enum):
    '''Target function type.
    '''
    TARGET_FLOOD = 1  # use min flood time as target.
    TARGET_PUMP = 2  # use min flood time and min pumps' cost as target.
    TARGET_SLUICE = 3  # use min flood time and max sluices' flow as target.
    TARGET_COMBINE = 4  # use min flood time, min pumps' cost and max sluices' flow as target.
    TARGET_ADD = 5  # add target above `TARGET_COMBINE` type.
    TARGET_QUAL = 6  # use quality as target only.   
    TARGET_CUSTOM = 7  # use costom target only.


class Operation(metaclass=ABCMeta):
    '''Operation Interface Class.

    To set up interfaces for operation module.
    '''

    @staticmethod
    def init(self, **modelFiles) -> bool:
        '''To initialize operation simulation. 
          
        Args/Attributes
        --------------------     
        modelFiles : dict.    
            To provid swmm model files, has keys "inFile", "rptFile", "outFile". 
            If "rptFile" and "outFile" unspecified, then "rpt_inFile" and "out_inFile" would
            be created under CWD.
          
        Return/Yeild:
        --------------------  
        True, if project opened sucessfully; 
        False, if project open failed, or specified invalid folder of "rptFile", "outFile".
        '''
        pass        

    @abstractmethod
    def run(self, mode=SimMode.SIM_COMBINE) -> bool:
        '''To run operation simulation. 

        Args/Attributes:
        --------------------
        mode : enum.
            To specify the simulation type, default is SIM_COMBINE.
          
        Return/Yeild:
        --------------------  
        True, if operation or swmm simulation done sucessfully; 
        False, the others.
        '''
        pass

    @abstractmethod
    def setTargetFunc(self, tType=TargetFuncType.TARGET_FLOOD, func=None):
        '''To set up operation target function.
          
        Args/Attributes:
        --------------------
        tType : enum.  
            The type of operation function, default is `TARGET_FLOOD`.
        func : function.  
            A scheme implement for the custom target, it taked list of objectes states
            as args. This must be provided if "type" specified as `TARGET_CUSTOM`.
          
        Returns/Yeilds:
        --------------------
        True, if operation scheme set up successfully; False, the others.
        '''
        pass

    @abstractmethod
    def setOperationPeriod(self, start, end) -> bool:
        '''To set up scheduling period.

        If simulation hasn't initialized, this setting is invalid.
        If inputting invalid period, whole simulation period would be used.
          
        Args/Attributes:
        --------------------
        start : list of int.
            To specify the begin datetime of operation.
        end : list of int.
            To specify the end datetime of operation.
          
        Returns/Yeilds:
        --------------------
        True, if inputing period valid; False, the others.
        '''
        pass

    @abstractmethod
    def setOperationStep(self, step) -> bool:
        '''To set up operation step.

        If simulation hasn't initialized, this setting is invalid.
          
        Args/Attributes:
        --------------------
        step : float.
            To specify the operation step (hour).
          
        Returns/Yeilds:
        --------------------
        True, if step is valid; False, the others.
        '''
        pass

    @abstractmethod
    def setOperationObjs(self, *objs) -> bool:
        '''To set operation structure objects.

        If simulation hasn't initialized, this setting is invalid.

        Args/Attributes:
        --------------------
        objIds : tuple.  
            The operation structure objects' id. if none specified, all pumps
            and sluices would be used.
          
        Returns/Yeilds:
        --------------------
        True, if all objs existed; False, the others.
        '''
        pass

    @abstractmethod
    def setTargetObjs(self, *objs) -> bool:
        '''To set up target node objects.

        If simulation hasn't initialized, this setting is invalid.
        To focus on key area and reduce computation by specify the target nodes
        that would be measured and evaluated. If none specified, then all nodes
        would be used.

        Args/Attributes:
        --------------------
        objIds : tuple.  
            The operating pump objects' id.
          
        Returns/Yeilds:
        --------------------
        True, if all objs existed; False, the others.
        '''
        pass        

    @abstractmethod
    def setConstraints(self, **constraints) -> bool:
        '''To set up operation simulation constraints. 
          
        Args/Attributes:
        --------------------
        constraints : dict.  
            To provid constraints, has keys "max_velocity","max_sluice_flow","max_pump_flow".
            The "max_sluice_flow", "max_pump_flow" are also dict.
            Keys may be extended by subclass. 
        
        Returns/Yeilds:
        --------------------
        True, if all constraints valid; False, the others.
        '''
        pass

    @abstractmethod
    def setPSOArgs(self, **psoArgs) -> bool:
        '''To set up args for sko.PSO. 
          
        Args/Attributes:
        --------------------
        psoArgs : dict.    
            To provides sko.PSO args "pop", "max_iter", "w", "c1", "c2".

        Returns/Yeilds:
        --------------------
        True, if all args are valid; False, the others.
        '''
        pass

    @abstractmethod
    def getOperationSolution(self) -> dict:
        '''To get the operation scheme.

        Return/Yeild:
        --------------------
        A dict contains the operation scheme, formatted as
        { 
            "pumps":{
                "pId1": [[t1, statue1], [t2, statue2], ...],
                ...
            },
            "sluices":{
                "sId1": [[t1, statue1], [t2, statue2], ...],
                ...
            },
        }
        '''
        pass

    @abstractmethod
    def getAllPumpIds(self) -> list:
        '''To get all ids of pumps in project.
        '''
        pass

    @abstractmethod
    def getAllSluiceIds(self) -> list:
        '''To get all ids of sluices in project.
        '''
        pass

    @abstractmethod
    def getAllOperationObjIds(self) -> list:
        '''To get all existed operation objects' ids.
        '''
        pass

    @abstractmethod
    def getTargetFuncVal(self) -> float:
        '''To get the optimal value of target function.
        '''
        pass

    @abstractmethod
    def getSimInfo(self) -> dict:
        '''To get statistical info about operation simulation.
          
        Returns/Yeilds:
        --------------------
        A dict mapping simulation status, formatted as
        {
            "swmmProject": {
                "datetime": [start, end],
                "routeStep": step,
                "nodes": nodeNum,
                "links": linkNum,
                "pumps": pumpNum,
                "sluices": sluiceNum, 
            },
            "psoArg": {
                "max_iter": maxIter,
                "pop": pop,
                "w": w,
                "c1": c1,
                "c2": c2,
                "dim": dim,
            },
            "simSet": {
                "period": [start, end],
                "operatinStep": step,
                "simMode": simMode,
                "targetType": targType,
                "targetNodes": targNodeNum,                
                "operatePumps": operPumpNum,
                "operateSluices": operSluiceNum,
            },
            "simRes": {
                "targetValue": target,
                "timeCost": timeCost,
            }
        }.
        '''
        pass


class PumpOperation(Operation):
    '''To run pumps optimal operation. 
    '''

    def __init__(self):
        '''To init pump eperation simualtion.
        '''
        self.__swmm = CDLL(fileTool.turnAbsolute("./core/swmm5.dll"))
        self.__files = {}  # swmm files in c_char_p.
        self.__errCode = 0  # swmm simualtion error code.  
        self.__isClosed = False  # flag indicated if swmm project unclosed.      
        logger.info("SWMM dll open.")

        self.__target = -1  # target function's optimal value.
        self.__targetType = TargetFuncType.TARGET_FLOOD  # target function's type.
        self.__cusFunc = None  # custom target function.
        self.__opeStep = 1  # operation step, default is 1 hour.
        self.__solution = {"pumps":{}, "sluices":{}}  # optimal operation scheme.

        self.__period = {}  # operation period.
        self.__psoArg = {}  # PSO args.
        self.__objs = {}  # operation pump objects.
        self.__tObjs = {}  # target node objects.

        self.__pumps = {}  # all pumps in swmm project.
        self.__sluices = {}  # all sluices in swmm project.
 
    def __del__(self):
        '''To close swmm project if not closed.
        '''
        if not self.__isClosed:
            self.__endSWMMProject()
            self.__closeSWMMProject()
            self.__isClosed = True
            logger.info("SWMM project cleaned.")
        logger.info("SWMM dll closed.")

    ####################################################################
    # Simulation process.
    # 
    # To split simulation process to steps. 
    #  
    def init(self, **modelFiles):
        '''To open swmm project.      
        '''
        # get swmm files.
        files = self.__checkSWMMFiles(**modelFiles)
        
        # open swmm project.
        cFiles = strTool.turnCharPointer(*(files.values()))
        if not self.__openSWMMProject(*cFiles):
            self.__files = {"inFile": "", "rptFile": "", "outFile": ""}
            logger.critical("SWMM open failed.")
            return False
        else:
            self.__files = {"inFile": cFiles[0], "rptFile": cFiles[1], "outFile": cFiles[2]}
            logger.info("SWMM project files checked.")
        
        # initialize simulation args.
        self.__initDefault()
        return True

    def run(self, mode=SimMode.SIM_COMBINE):
        '''To run pumps operation simulation.
        '''  
        # check state and set default for simulation.
        self.__preLoop(mode)

        # check if simulation mode valid.
        if mode not in SimMode.__members__.values():
            logger.critical("Simulation mode({}) not supported".format(mode))
            return False
       
        # run swmm if SIM_SWMM.
        if mode == SimMode.SIM_SWMM:
            if self.__runSWMMProject(*(self.__files.values())):
                return True
            else:
                return False

        # run pso.
        pso = PSO(**self.__psoArg)
        pso.run()
        self.__target = pso.gbest_y 
        self.__parseOptimalScheme(mode, pso.gbest_x)

        # run operation simualtion.
        if self.__target < 0:
            return False
        else:
            return True

    def __initDefault(self):
        '''To set default value for simualtion.
        '''
        # set default scheduling period.
        start = self.__getDateTime(0)
        end = self.__getDateTime(1)
        self.__period = {"begin": start, "end": end}    

        # set default scheduling pumps objects.
        pumpIds = self.__getAllStructures("pump")
        for Id in pumpIds:
            Ind = self.__findObjectIndex(3, Id) 
            self.__pumps[Id] = Ind   
            self.__objs[Id] = Ind

        # set default scheduling sluices objects.
        sluiceIds = self.__getAllStructures("sluice")
        for Id in sluiceIds:
            Ind = self.__findObjectIndex(3, Id) 
            self.__sluices[Id] = Ind
            # self.__objs[Id] = Ind   # don't consider sluice

        # set default target nodes objects.
        allNodes = self.__countObjects(2).value
        for Ind in range(0,allNodes):
            Id = self.__findObjectId(2, Ind)
            self.__tObjs[Id] = Ind
        
        # check and set pso args.
        self.__psoArg["c2"] = 0.5
        self.__psoArg["c1"] = 0.5
        self.__psoArg["w"] = 0.8
        self.__psoArg["max_iter"] = 200
        self.__psoArg["pop"] = 50
        self.__psoArg["dim"] = 0
        self.__psoArg["lb"] = []
        self.__psoArg["ub"] = []        
        self.__psoArg["func"] = self.__scheme

    def __preLoop(self, simMode):
        '''To close swmm project opened and set pso args. 

        Args/Attributes:
        --------------------
        simMode : int. 
            The simulation mode, select objects in operation objs.
        '''
        # set pso args "func", "dim", "lb", "ub".
        self.__generatePSODimension(simMode)
        if self.__cusFunc: 
            if self.targetType == TargetFuncType.TARGET_CUSTOM or \
                self.targetType == TargetFuncType.TARGET_ADD:
                self.__psoArg["func"] = self.__cusFunc

        # check and reset swmm project error code.
        if self.__errCode > 0:
            logger.critical("SWMM project state() unnormal.".format(self.__errCode))
            self.__closeSWMMProject()
            self.__isClosed = True
            return 
        if not self.__isClosed:
            self.__endSWMMProject()
            self.__closeSWMMProject()
            self.__isClosed = True

        # clean result statu.
        self.__errCode = 0 
        self.__target = -1
        self.__solution = {"pumps":{}, "sluices":{}}

    def __parseOptimalScheme(self, mode, scheme):
        '''To parse pump optimal scheme to output format.
        '''
        # check simulation mode.
        if mode == SimMode.SIM_SWMM:
            return 

        # match index with id after sorted.
        Ids, Inds = [], []
        for k, v in self.__objs.items():
            Ids.append(k)
            Inds.append(v.value)
        sortedInds = sorted(Inds)  # sorted index.
        sortedIds = [Ids[Inds.index(i)] for i in sortedInds]

        # generate scheduling timeseries.
        ts, currDate =[], self.__period["begin"]
        while timeTool.isEarlier(currDate, self.__period["end"]):
            strDate = timeTool.getDateFormatted(currDate)
            ts.append(strDate)
            currDate = timeTool.updateTime(currDate, "add", hours=self.__opeStep)

        # get all sheduling pumps or sluices(ignored).
        pumpIds = self.__getAllStructures("pump")
        sluiceIds = self.__getAllStructures("sluice")

        # generate pumps scheduling timeseries.
        pumpNum = len(self.__solution["pumps"])
        periodLen = len(ts)
        settingInd = 0
        for Id, Ind in zip(sortedIds, sortedInds):
            if mode == SimMode.SIM_SLUICE and Id in pumpIds:
                continue
            elif mode == SimMode.SIM_PUMP and Id in sluiceIds:
                continue
            for t in ts:
                self.__solution["pumps"][Id].append([t, scheme[settingInd]])
                settingInd += 1

    ####################################################################
    # Simulation setting methods.
    # 
    # To set up simulation settings for operation after `init()`.
    #
    def setTargetFunc(self, tType=TargetFuncType.TARGET_FLOOD, func=None):
        '''To set up pump's operation target.
        '''
        if tType not in TargetFuncType.__members__.values():
            logger.error("Operation target type({}) is invalid.".format(tType))
            return False
        elif tType == TargetFuncType.TARGET_SLUICE:
            logger.error("Pump operation target type({}) is invalid.".format(tType))
            return False
        elif tType == TargetFuncType.TARGET_CUSTOM and func is None:
            logger.error("Operation target custom function is None.")
            return False
        elif tType == TargetFuncType.TARGET_ADD and func is None:
            logger.error("Operation target added function is None.")
            return False
        else:
            self.__targetType = tType
            self.__cusFunc = func
            logger.info("Pump operation target set up.")
            return True

    def setOperationObjs(self, *objIds):
        '''To set operating pump objects.
        '''
        # check swmm project state.
        # if self.__errCode != 0:
        #     logger.critical("SWMM project state unnormal while set operation objs.")
        #     return False

        # find pump(type=3) objects if existed.
        objs, invalid_objs ={}, []
        for Id in objIds:
            objInd = self.__findObjectIndex(3, Id)
            if objInd is None:
                invalid_objs.append(Id)
            else:
                objs[Id] = objInd

        # check if there are invalid ids.
        if invalid_objs:
            logger.warning("Operation objs({}) not existed.".format(invalid_objs))
            return False
        else:
            # reset operaiton pumps.
            self.__objs = objs
            logger.info("Operation objs set up.")
            return True

    def setTargetObjs(self, *objs):
        # check swmm project state.
        # if self.__errCode != 0:
        #     logger.critical("SWMM project state unnormal while set operation objs.")
        #     return False
        
        # find node(type=2) objects if existed.
        tObjs, invalid_objs ={}, []
        for Id in objs:
            objInd = self.__findObjectIndex(2, Id)
            if objInd is None:
                invalid_objs.append(Id)
            else:
                tObjs[Id] = objInd
        
        # check if there are invalid ids.
        if invalid_objs:
            logger.warning("Target objs({}) not existed.".format(invalid_objs))
            return False
        else:
            self.__tObjs = tObjs
            logger.info("Target objs set up.")
            return True

    def setOperationPeriod(self, start, end):
        '''To set pump operation period.
        '''
        # check swmm project state.
        # if self.__errCode != 0:
        #     logger.critical("SWMM project state unnormal while set operation period.")
        #     return False 

        # get swmm project datetime.
        swmmStart = self.__getDateTime(0)
        swmmEnd = self.__getDateTime(1)           

        # check inputing period if valid.
        period = timeTool.isEarlier(start, end)
        startCheck = timeTool.isEarlier(swmmStart, start)
        endCheck = timeTool.isEarlier(end, swmmEnd)

        if not period  or not startCheck  or not endCheck:      
            logger.error("Operation period({} to {}) is invalid".format(start, end))
            return False
        else:
            self.__period = {"begin": start, "end": end}
            logger.info("Operation period set up.")
            return True

    def setOperationStep(self, step):
        '''To set up operation step.

        `int swmm_getSimulationParam(int type, double *value)`
        '''
        # check swmm project state.
        # if self.__errCode != 0:
        #     logger.critical("SWMM project state unnormal while set operation step.")
        #     return False 

        # declare swmm api.
        getSimStep = self.__swmm.swmm_getSimulationParam
        getSimStep.argtypes = [c_int, POINTER(c_float)]
        getSimStep.resType = c_int

        # get route step(type=0) of swmm project.
        routeStep = c_float(0)
        self.__errCode = getSimStep(c_int(0), routeStep)

        # check if operation step is valid.
        if step*3600 < routeStep.value:
            logger.warnning("Operation step({}) short then route step".format(step))
            return False
        else:
            self.__opeStep = step
            logger.info("Operation step set up.")
            return True

    def setConstraints(self, **constraints):
        '''To set simulation constraints.

        Closed.
        '''
        pass

    def setPSOArgs(self, **psoArgs):
        '''To set up args for sko.PSO. 

        If some of keys not being specified, default value would be used.
        If setting args mixed with valid, invalid and unknown args, then
        valid args would be used and return False.

        Args/Attributes:
        --------------------
        psoArgs : dict.    
            To provides sko.PSO args "pop", "max_iter", "w", "c1", "c2".
            Default, pop=50, max_iter=200, w=0.8, c1=c2=0.5.
        '''
        invalid_args, unknown_args = [], []

        # get c2.
        if "c2" in psoArgs:
            if psoArgs["c2"] > 0:
                self.__psoArg["c2"] = psoArgs["c2"]
            else:
                invalid_args.append("c2")

        # get c1.
        if "c1" in psoArgs:
            if psoArgs["c1"] > 0:
                self.__psoArg["c1"] = psoArgs["c1"]
            else:
                invalid_args.append("c1")

        # get w.
        if "w" in psoArgs:
            if 0 < psoArgs["w"] < 1.0:
                self.__psoArg["w"] = psoArgs["w"]
            else:
                invalid_args.append("w")

        # get max_iter.
        if "max_iter" in psoArgs:
            if psoArgs["max_iter"] > 0:
                self.__psoArg["max_iter"] = int(psoArgs["max_iter"])
            else:
                invalid_args.append("max_iter")

        # get pop.
        if "pop" in psoArgs:
            if psoArgs["pop"] > 0:
                self.__psoArg["pop"] = int(psoArgs["pop"])
            else:
                invalid_args.append("pop")

        # check if unknown item.
        for key in psoArgs:
            if key not in ["pop", "max_iter", "w", "c1", "c2", "func", "dim", "lb", "ub"]:
                unknown_args.append(key)
        if unknown_args:
            logger.warning("PSO set unknown item({}).".format(unknown_args))
            return False
        
        # check if invalid item.
        for key in psoArgs:
            if key in ["func", "dim", "lb", "ub"]:
                invalid_args.append(key)

        # check if all args valid.
        if invalid_args:
            logger.error("PSO set args({}) invalid".format(invalid_args))
            return False
        else:
            logger.info("PSO args set up.")
            return True

    ####################################################################
    # Output methods.
    # 
    # To get result from operation simualtion.
    #        
    def getOperationSolution(self):
        '''To return operation scheme.
        '''
        return self.__solution

    def getTargetFuncVal(self):
        return self.__target

    def getAllOperationObjIds(self):
        return self.__objs.keys()

    def getAllPumpIds(self):
        return self.__pumps.keys()

    def getAllSluiceIds(self):
        return self.__sluices.keys()

    def getSimInfo(self):
        pass

    ####################################################################
    # Pumps operation algorithm.
    # 
    # To implement the optimal operation of pumps.
    #
    def __generatePSODimension(self, simMode):
        '''To generate PSO dimension and its low-boundary and upper-boundary.

        Num, means number of pumps; 
        Len, means steps equaled to operation period divided by operation step;
        The operation dimension: Num * Len.

        The "lb" is formatted as: 
        [
            pump1_lb_t1, pump2_lb_t1, ...,  # while t = t1.
            pump1_lb_t2, pump2_lb_t2, ...,  # while t = t2.
            ...
        ] 
        The "ub" is the same.

        Args/Attributes:
        --------------------
        simMode : int. 
            The simulation mode, select objects in operation objs.
        '''
        # check simulation mode.
        if simMode == SimMode.SIM_SWMM:
            return 

        # generate dimension and its lb ,ub.
        t = self.__period["begin"]
        while timeTool.calcTimeInterval(t, self.__period["end"]) > 0:
            # force uniform operation objs' order by sorting index. 
            for i in sorted(self.__objs.valus()):
                if simMode == SimMode.SIM_PUMP:
                    if i not in self.__pumps.values():
                        continue
                elif simMode == SimMode.SIM_SLUICE:
                    if i not in self.__sluices.values():
                        continue
                self.__psoArg["lb"].append(0)
                self.__psoArg["ub"].append(1)
            t = timeTool.updateTime(t, "add", hours=self.__opeStep)
        self.__psoArg["dim"] = len(self.__psoArg["lb"])

    def __setStructureState(self, objInd, value):
        '''To set pump or sluice state.

        `int swmm_setLinkSetting(int index, double setting)`

        Args/Attributes:
        --------------------
        objInd : int. 
            The object index.
        value : float.  
            The object setting.

        Return/Yield:
        --------------------
        True, if setted successfully; False, the others. 
        '''
        # declare swmm api.
        setValue = self.__swmm.swmm_setLinkSetting
        setValue.argtypes = [c_int, c_float]
        setValue.resType = c_int

        # set object setting.
        self.__errCode = setValue(c_int(objInd), c_float(value)).value
        if self.__errCode > 0:
            logger.error("Operation state({}, {}, {}) sets failed.".format(
                self.__getCurrDateTime(), self.__findObjectId(3, objInd), value
            ))
            return False
        else:
            return True

    def __calcPumpCosts(self):
        '''To calculate pumps costs.

        Closed.
        '''
        pass

    def __calcNodeFloodTime(self):
        '''To calculate target nodes's total flood time.

        `int swmm_getNodeStats(int index, SM_NodeStats *nodeStats)`
        '''
        # create swmm nodestate structure.
        class NodeStats(Structure):
            '''SWMM NodeStats structure.
            '''
            _fields_ = [
                ("avgDepth"          , c_float), 
                ("maxDepth"          , c_float),
                ("maxDepthDate"      , c_float),
                ("maxRptDepth"       , c_float),
                ("volFlooded"        , c_float),
                ("timeFlooded"       , c_float),
                ("timeSurcharged"    , c_float),
                ("timeCourantCritic" , c_float),
                ("totLatFlow"        , c_float),
                ("maxLatFlow"        , c_float),
                ("maxInflow"         , c_float),
                ("maxOverflow"       , c_float),
                ("maxPondedVol"      , c_float),
                ("maxInflowDate"     , c_float),
                ("maxOverflowDate"   , c_float),                  
            ]
        state = NodeStats()

        # declare swmm api.
        getStats = self.__swmm.swmm_getNodeStats
        getStats.argtypes = [NodeStats]
        getStats.restype = c_int

        # calculate total surcharge time.
        floodTime = 0
        for ind in self.__tObjs.values():
            stats = NodeStats()
            if getStats(stats) > 0:
                floodTime += stats.timeFlooded
        return floodTime

    def __calcSluiceFlow(self):
        '''To calculate sluices flow.
        '''
        pass

    def __calcQuality(self):
        '''To calculate water quality stuff.
        '''
        pass

    def __calcTarget(self):
        '''To calculate target function value.
        '''
        target = 0
        if self.__targetType == TargetFuncType.TARGET_FLOOD:
            target = self.__calcNodeFloodTime()
        elif self.__targetType == TargetFuncType.TARGET_PUMP:
            target += self.__calcNodeFloodTime()
            target += self.__calcPumpCosts()
        elif self.__targetType == TargetFuncType.TARGET_SLUICE:
            target += self.__calcNodeFloodTime()
            target += self.__calcSluiceFlow()
        elif self.__targetType == TargetFuncType.TARGET_COMBINE:
            target += self.__calcNodeFloodTime()
            target += self.__calcPumpCosts()
            target += self.__calcSluiceFlow()
        elif self.__targetType == TargetFuncType.TARGET_ADD:
            target += self.__calcNodeFloodTime()
            target += self.__calcPumpCosts()
            target += self.__calcSluiceFlow()
            target += self.__cusFunc(self.__swmm)
        elif self.__targetType == TargetFuncType.TARGET_CUSTOM:
            target += self.__cusFunc(self.__swmm)
        elif self.__targetType == TargetFuncType.TARGET_QUAL:
            target += self.__calcQuality()
        return target

    def __scheme(self, settings):
        '''To set operation target.

        Now only `TARGET_FLOOD` is supported.

        Args/Attributes:
        --------------------
        settings : list of float.    
            To provides pumps settings at each step.

        Returns/Yeilds:
        --------------------
        Target function value(>= 0), if simulation done successfully;
        -1, the others.
        '''     
        # check target type(only `TARGET_FLOOD` supported now).
        if self.__targetType not in TargetFuncType.__members__.values():
            logger.error("Target func type({}) invalid.".format(TargetFuncType.__members__))
            return -1

        # run pumps simulation.
        self.__isClosed = False
        if not self.__openSWMMProject(*(self.__files.values())):            
            return -1

        self.__errCode = self.__swmm.swmm_start(c_bool(True))
        if self.__errCode > 0:
            logger.debug("SWMM project start failed.")
            return -1
        
        elapsedTime = c_float(0.0)  # simualation time remaining.
        date = self.__period["begin"]  # current scheduling date time.
        settingInd = 0  # current setting index.
        while elapsedTime.value > 0.0 and self.__errCode == 0:
            currDate = self.__getCurrDateTime()
            # check if in scheduling period and at scheduling step.
            if timeTool.isEarlier(self.__period["start"], currDate) and \
                timeTool.isEarlier(currDate, self.__period["end"]) and \
                timeTool.isEarlier(date, currDate):
                # set scheduling pumps setting.
                for i in sorted(self.__objs.values()):
                    self.__setStructureState(i, settings[settingInd])
                    settingInd += 1
                # update datetime to next scheduling step.
                date = timeTool.updateTime(date, "add", hours=self.__opeStep)
            self.__errCode = self.__swmm.swmm_step(byref(elapsedTime))

        # calculate target.
        target = self.__calcTarget()

        # close swmm project.
        self.__errCode = self.__swmm.swmm_end()
        self.__swmm.swmm_close()
        self.__isClosed = True
        if self.__errCode > 0:
            logger.warning("SWMM project end failed.")
            return -1
        else:
            self.__isClosed = True
            logger.debug("Operation simualtion scheme run.")
            return target

    ####################################################################
    # Simulation utils.
    # 
    # To provid objects search, count functions, and so on.
    #
    def __runSWMMProject(self, *cFiles):
        '''To run swmm project.

        `int swmm_run(const char *f1, const char *f2, const char *f3)`
        '''
        # check if all swmm files existed.
        if len(cFiles) < 3:
            logger.error("SWMM project files missed.")
            return False

        # declare swmm api.
        openSWMM = self.__swmm.swmm_run
        openSWMM.argtypes = [c_char_p, c_char_p, c_char_p]
        openSWMM.restype = c_int

        # open swmm project.
        self.__errCode = openSWMM(cFiles[0], cFiles[1], cFiles[2])
        if self.__errCode > 0:
            logger.critical("SWMM project run failed.")
            return False
        else:
            logger.info("SWMM project run.")
            return True

    def __openSWMMProject(self, *cFiles):
        '''To open swmm project.

        `int swmm_open(const char *f1, const char *f2, const char *f3)`
        '''
        # check if swmm files existed.
        if len(cFiles) < 3:
            logger.error("SWMM project files missed.")
            return False

        # declare swmm api.
        openSWMM = self.__swmm.swmm_open
        openSWMM.argtypes = [c_char_p, c_char_p, c_char_p]
        openSWMM.restype = c_int

        # open swmm project.
        self.__errCode = openSWMM(cFiles[0], cFiles[1], cFiles[2])
        if self.__errCode > 0:
            logger.critical("SWMM project open failed.")
            return False
        else:
            logger.debug("SWMM project open.")
            return True
 
    def __endSWMMProject(self):
        self.__swmm.swmm_end()

    def __closeSWMMProject(self):
        self.__swmm.swmm_close()

    def __checkSWMMFiles(self, **files):
        '''To check swmm files.
        '''
        swmmFiles = {"inFile": "", "rptFile": "", "outFile": ""}

         # get swmm input file.
        if "inFile" in files and fileTool.isExisted(files["inFile"]):
            swmmFiles["inFile"] = files["inFile"]
        else:
            return swmmFiles

        # get swmm report file.
        if "rptFile" in files and fileTool.isExisted(fileTool.getParentDir(files["rptFile"])):
            swmmFiles["rptFile"] = files["rptFile"]
        else:
            direction = fileTool.getParentDir(files["inFile"])
            swmmFiles["rptFile"] = fileTool.createFile("report.inp", direction)

        # get swmm output file.
        if "outFile" in files and fileTool.isExisted(fileTool.getParentDir(files["outFile"])):
            swmmFiles["outFile"] = files["outFile"]
        else:
            swmmFiles["outFile"] = ""
        return swmmFiles

    def __findObjectIndex(self, objType, objId):
        '''To find object index in swmm project.

        `int swmm_project_findObject(int type, char *id, int *index)`

        Args/Attributes:
        --------------------
        objType : enum. 
            The type of object.
        objId : str.  
            The objects' id.

        Return/Yield:
        --------------------
        Object index, if object existed; None, the others. 
        '''
        # check if valid object type.
        if 0 > objType or objType > 15:
            logger.error("SWMM objtype({}) is invalid.".format(objType))
            return None

        # declare swmm api. 
        findObject = self.__swmm.swmm_project_findObject
        findObject.argtypes = [c_int, c_char_p, POINTER(c_int)]
        findObject.restype  = c_int
        
        # find object.
        objInd = c_int(-1)
        self.__errCode = findObject(c_int(objType), c_char_p(objId.encode()), objInd)
        if self.__errCode > 0:
            return None
        else:
            return objInd

    def __findObjectId(self, objType, objInd):
        '''To find object id in swmm project.

        `int swmm_getObjectId(int type, int index, char *id)`

        Args/Attributes:
        --------------------
        objType : enum. 
            The type of object.
        objInd : int.  
            The objects' index.

        Return/Yield:
        --------------------
        Object id, if object existed; None, the others. 
        '''
        # check if valid object type.
        if 0 > objType or objType > 15:
            logger.error("SWMM objtype({}) is invalid.".format(objType))
            return None
        
        # declare swmm api.
        getId = self.__swmm.swmm_getObjectId
        getId.argtypes = [c_int, c_int, c_char_p]
        getId.restype = c_int

        # get object id.
        objId = c_char_p(b"")
        self.__errCode = getId(c_int(objType), c_int(objInd), objId)
        if self.__errCode > 0:
            return None
        else:
            return objId.value.decode()

    def __getDateTime(self, timeType):
        '''To get swmm simulation datetime.

        `int swmm_getSimulationDateTime(
            int timetype, int *year, int *month, int *day, 
            int *hour, int *minute, int *second
            )
        `

        Args/Attributes:
        --------------------
        timeType : enum.  
            The datetime type.

        Return/Yield:
        --------------------
        Datetime(list of int), if timeType correct; None, the others. 
        '''    
        # check if valid time type.
        if 0 > timeType or timeType > 2:
            logger.error("SWMM timetype({}) is invalid.".format(timeType))
            return None

        # set swmm datetime api.
        getDateTime = self.__swmm.swmm_getSimulationDateTime
        getDateTime.argtypes = [c_int, POINTER(c_int), POINTER(c_int), POINTER(c_int), 
        POINTER(c_int), POINTER(c_int), POINTER(c_int)]
        getDateTime.restypes = c_int

        # get swmm project datetime.
        year, mon, day = c_int(-1), c_int(-1), c_int(-1)
        hour, minu, sec = c_int(-1), c_int(-1), c_int(-1)
        self.__errCode = getDateTime(c_int(timeType), year, mon, day, hour, minu, sec)
        if self.__errCode > 0:
            logger.error("SWMM datetime({}) acquisition failed.".format(timeType))
            return None
        else:
            # turn c_long to int.
            return [year.value, mon.value, day.value, hour.value, minu.value, sec.value]

    def __getCurrDateTime(self):    
        '''To get current swmm simulation datetime.

        `int swmm_getCurrentDateTime(
            int *year, int *month, int *day, 
            int *hour, int *minute, int *second
            )
        `

        Return/Yield:
        --------------------
        Datetime(list of int). 
        '''    
        # set swmm datetime api.
        getDateTime = self.__swmm.swmm_getCurrentDateTime
        getDateTime.argtypes = [POINTER(c_int), POINTER(c_int), POINTER(c_int), 
        POINTER(c_int), POINTER(c_int), POINTER(c_int)]
        getDateTime.restypes = c_int

        # get swmm project datetime.
        year, mon, day = c_int(-1), c_int(-1), c_int(-1)
        hour, minu, sec = c_int(-1), c_int(-1), c_int(-1)
        self.__errCode = getDateTime(year, mon, day, hour, minu, sec).value
        if self.__errCode > 0:
            logger.error("SWMM current datetime acquisition failed.")
            return None
        else:
            return [year.value, mon.value, day.value, hour.value, minu.value, sec.value]

    def __getAllStructures(self, objType):
        '''To count all pumps or sluices of links in swmm project.

        `int swmm_getLinkType(int index, int *Ltype)` 

        Args/Attributes:
        --------------------
        objType : str.
            The type of object, "pump" means pump objects, "sluice" is the same.
          
        Returns/Yeilds:
        --------------------
        All specified type objects' id.
        '''
        # check if valid object type.
        if objType not in ["pump", "sluice"]:
            logger.error("SWMM project has no objects({})".format(objType))
            return None
        
        # count all links.
        linkNum = self.__countObjects(3).value
        if linkNum < 1:
            logger.critical("SWMM project has no links.")
            return None
        
        # declare swmm api.
        getLinkType = self.__swmm.swmm_getLinkType
        getLinkType.argtypes = [c_int, POINTER(c_int)]
        getLinkType.restype = c_int
 
        # count structures, pump(type=1, orifice=2).
        objs = []
        for i in range(0, linkNum):
            linkType = c_int(-1)
            getLinkType(i, linkType)
            if objType == "pump" and linkType.value == 1:
                objs.append(self.__findObjectId(3, i))
            elif objType == "sluice" and linkType.value == 2:
                objs.append(self.__findObjectId(3, i))
        return objs

    def __countObjects(self, objType):
        '''To count all objects of specified type in swmm project.

        `int swmm_countObjects(int type, int *count)` 

        Args/Attributes:
        --------------------
        objType : str.
            The type of object.
          
        Returns/Yeilds:
        --------------------
        The number of specified type objects.
        '''
        # check if valid object type.
        if 0 > objType or objType > 15:
            logger.error("SWMM objType({}) is invallid".format(objType))
            return None
        
        # declare swmm api.
        countObjects = self.__swmm.swmm_countObjects
        countObjects.argtypes = [c_int, POINTER(c_int)]
        countObjects.restype = c_int

        # count all objects.
        objNum = c_int(0)
        self.__errCode = countObjects(c_int(objType), objNum)
        if self.__errCode > 0:
            logger.warnning("SWMM project has no objects of type({})".format(objType))
            return 0
        else:
            return objNum
