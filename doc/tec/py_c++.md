# Python -- C++
python 调用 c++ 动态库法操作方法。  

**`import ctypes`**

--------------------------------------------------------------------------------------

## case1. 传入实数调用demo， int add1(int a, int b)

```PYTHON 
result1 = dll.add1(2,3)
print("传入实数调用demo:")
print(result1)  
```

--------------------------------------------------------------------------------------

## case2. 传入指针调用demo， void add2(int a, int b, int *c)

```PYTHON  
result2 = ctypes.c_int(0)              #定义int类型变量，初始值为0
dll.add2(4, 5, ctypes.byref(result2))  #byref指明参数传递时为引用传递，对应C的指针传递

print("传入指针调用demo:")
print(result2.value)                   #value可以查看变量的值
```

--------------------------------------------------------------------------------------

## case3. 传入指针调用demo， void add3(int *a, int *b, int *c)

```PYTHON
a = ctypes.c_int(6)
b = ctypes.c_int(7)
# 方法一
result3_1 = ctypes.c_int(0)
dll.add3(ctypes.byref(a), ctypes.byref(b), ctypes.byref(result3_1))
# 方法二
result3_2 = ctypes.c_int(0)
add3 = dll.add3
add3.argtypes = [ctypes.POINTER(ctypes.c_int),ctypes.POINTER(ctypes.c_int),ctypes.POINTER(types.c_int)]
add3(a,b,result3_2)
```

--------------------------------------------------------------------------------------

## case4. 传入字符串调用demo， int get_str_length(char *in_str)

```PYTHON
    p_str = ctypes.c_char_p(b'hello')          # or p_str =  b'hello'

    # 方法一
    str_length1 = dll.get_str_length(p_str)

    # 方法二
    get_str_length = dll.get_str_length
    get_str_length.argtypes = [ctypes.c_char_p]
    get_str_length.restype  = ctypes.c_int
    str_length2 = get_str_length(p_str)
```

--------------------------------------------------------------------------------------

## case5. 传入空字符串指针，返回字符串demo,  char* get_string(char *in_str)

```PYTHON
    get_string = dll.get_string
    get_string.argtypes = [ctypes.c_char_p]
    get_string.restype  = ctypes.c_char_p

    in_str  = ctypes.create_string_buffer(1024,'\0')
    out_str = get_string(in_str)

    print("传入空字符串指针，返回字符串demo:")
    print(in_str.value)
    print(out_str)

```
--------------------------------------------------------------------------------------

## case6. 结构体参数.

```C++
# include <stdio.h>
# include <stdlib.h>

//创建一个Student结构体 
struct Student
{
    char name[30];
    float fScore[3];
};

void Display(struct Student su)
{
    printf("-----Information------\n");
    printf("Name:%s\n",su.name);
    printf("Chinese:%.2f\n",su.fScore[0]);
    printf("Math:%.2f\n",su.fScore[1]);
    printf("English:%.2f\n",su.fScore[2]);
    printf("总分数为:%f\n", su.fScore[0]+su.fScore[1]+su.fScore[2]);
    printf("平均分数为:%.2f\n",((su.fScore[0]+su.fScore[1]+su.fScore[2]))/3);
}
```

```PYTHON
import ctypes
from ctypes import *

class Student(Structure):
    _fields_ = [
        ("name",c_char * 30),    # 声明长度为30的字符数组
        ("fScore", c_float * 3)  # 声明长度为3的浮点数组
        ]    

# 创建一个结构体
su = Student()

# 创建字符串
su.name = b"test-sdk"

# 创建浮点数组
PARAM = c_float * 3
fScore = PARAM()
fScore[0] = 55.1
fScore[1] = 33.2
fScore[2] = 51.3
su.fScore = fScore

if __name__ == '__main__':
    
    ll = ctypes.cdll.LoadLibrary   
    lib = ll("./libpycall.so") 
    # lib.Display.restype = c_float
    lib.Display.argtypes = [Student]    
    lib.Display(su)  
```
--------------------------------------------------------------------------------------







