# PSO Algorithm Introduction
粒子群算法(particle swarm optimization)。

---------------------------------------------

## 参数

`scikit-opt.PSO`

    **入参**	   **默认值**      **意义**
      func           -	         目标函数
      n_dim	         -	         目标函数的维度
      size_pop	     50	         种群规模
      max_iter	     200	       最大迭代次数
      lb	           None	       每个参数的最小值
      ub	           None	       每个参数的最大值
      w	             0.8	       惯性权重
      c1	           0.5	       个体记忆
      c2	           0.5	       集体记忆

---------------------------------------------

## 参数配置

### 种群规模
种群规模影响着算法的搜索能力和计算量：  
PSO对种群规模要求不高，一般取20—40就可以达到很好的求解效果。   
对于比较难的问题或者特定类别的问题，粒子数可以取到100或200。

---------------------------------------------

### 目标函数的维度与参数范围
目标函数的维度由优化问题本身决定，就是问题解的长度。    
参数范围由优化问题本身决定，每一维可以设定不同的范围。

---------------------------------------------

### 惯性权重
惯性权重控制着前一速度对当前速度的影响，用于平衡算法的探索和开发能力。  
一般设置为从0.9线性递减到0.4，也有非线性递减的设置方素；    
可以采用模糊控制的方式设定，或者在[0.5， 1.0]之间随机取值。

---------------------------------------------

### 加速系数
加速系数c1和c2代表了粒子向自身极值pBest和全局极值gBest推进的加速权值。    
c1和c2通常都等于2.0，代表着对两个引导方向的同等重视；c1和c2也可以设置不相等，但其范围一般都在0和4之间。

---------------------------------------------

### 终止条件
终止条件决定算法运行的结束，由具体的应用和问题本身确定。  
将最大循环数设定为500， 1000， 5000，或者最大的函数评估次数，等等。

---------------------------------------------
