# pyswmm_dev


To optimize the combined operation of sluices and pumps in urban rive and drain-pip network. 
优化城市河网和排水管网中闸泵的防洪调度。

[OurLand](./img/1.jpg)  
[ZhiHu](https://www.zhihu.com "zhihu")


# Chapter

To write some chapters.

>" Quote: There is some words that said from some famous people. "

-----------------------------------------
    
## Section

To write some sections.  

Some unorderd list are listed here:
+ line1
  + line1-1
  + line1-2
  + line1-3 
+ line2
+ line3


some ordered list are listed here:
1. line1
    + 1-1. line1-2
    + 1-2. line1-2
2. line2
    + line2-1
    + line2-2
3. line3
-----------------------------------------


### Paragraph

To write some paragraphes.  

```c++

    int main(int argc, const char* argv[])
    {
        std::cout<<"hello world."<<std::endl;
        return 0;
    }
```

Some sentences in this md contains code &emsp; `print("This is py code")` &emsp; for example.

-----------------------------------------

### Photo

To add some photos.  

![NewLand](./img/3.gif)

Here is a picture named *New Land* that is really nice!  
For the first time i see it, i **loved** it.

<img src="./img/2.jpg" alt="New Land" title="New Land" width="640" height="260" >

-----------------------------------------
&nbsp;  
&nbsp;

# Advance

To provide advanced function.

-----------------------------------------
&nbsp;


## Want to add non-function part

Want to add empty space, use `&emsp;`  
Want to add blank line,  use `&nbsp;`

-----------------------------------------
&nbsp;

## Want to add table
:--- 代表左对齐&emsp;&emsp;
:--: 代表居中对齐&emsp;&emsp;
---: 代表右对齐

| left | center | right |
| :--- | :----: | ----: |
| aaaa | bbbbbb | ccccc |
| a    | b      | c     |

|     name     | age |             blog                |
| ------------ | --- | ------------------------------- |
| _LearnShare_ |  12 | [LearnShare](http://xianbai.me) |
| __Mike__     |  32 | [Mike](http://mike.me)          |

-----------------------------------------
&nbsp;

## Want to add chart

```mermaid
graph LR
A[方形] -->B(圆角)
    B --> C{条件a}
    C -->|a=1| D[结果1]
    C -->|a=2| E[结果2]
    F[横向流程图]
```

```mermaid
graph TD
A[方形] -->B(圆角)
    B --> C{条件a}
    C -->|a=1| D[结果1]
    C -->|a=2| E[结果2]
    F[竖向流程图]
```

```mermaid
%% 时序图例子,-> 直线，-->虚线，->>实线箭头
  sequenceDiagram
    participant 张三
    participant 李四
    张三->王五: 王五你好吗？
    loop 健康检查
        王五->王五: 与疾病战斗
    end
    Note right of 王五: 合理 食物 <br/>看医生...
    李四-->>张三: 很好!
    王五->李四: 你怎么样?
    李四-->王五: 很好!
``` 


```mermaid
gantt
title 甘特图
dateFormat YYYY-MM-DD
section 行1
任务1 :a1, 2014-01-01, 30d
任务2 :after a1 , 20d
section 行2
任务3 :2014-01-12 , 12d
任务4 : 24d
```

```mermaid
classDiagram
Class01 <|-- AveryLongClass : Cool
Class03 *-- Class04
Class05 o-- Class06
Class07 .. Class08
Class09 --> C2 : Where am i?
Class09 --* C3
Class09 --|> Class07
Class07 : equals()
Class07 : Object[] elementData
Class01 : size()
Class01 : int chimp
Class01 : int gorilla
Class08 <--> C2: Cool label
```
