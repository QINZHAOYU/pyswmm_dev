# PYTHON CODE STYLE GUIDE 
代码规范（GOOGLE）。  
&nbsp;

-----------------------------------------------------

## 1. Format Style
代码格式规范，包括行长度、长字符串链接、缩进等方面。   
&nbsp;

### 1.1 行长度：每行不超过80个字符
### 1.2 长字符串链接：不要用 \\链接，使用 ()实现隐式链接
### 1.3 缩进：使用统一使用4个空格，禁止空格和TAB混用
### 1.4 空行：顶级定义之间空两行，方法或内部空一行  

-----------------------------------------------------
&nbsp;  

## 2. Annotation
代码注释规范， 包括函数和类的`pydoc`格式注释、内部注释等方面。  

-----------------------------------------------------

### 2.1 函数和方法的注释
函数,包括函数, 方法, 以及生成器。

文档字符串应该包含函数做什么, 以及输入和输出的详细描述. 通常, 不应该描述"怎么做", 除非是一些复杂的算法。  
对于复杂的代码, 在代码旁边加注释会比使用文档字符串更有意义。

每节应该以一个标题行开始，标题行以冒号结尾。 

**Args**:  
列出每个参数的名字, 并在名字后使用一个冒号和一个空格, 分隔对该参数的描述。 描述应该包括所需的类型和含义。   
如果一个函数接受`*foo(可变长度参数列表)`或者`**bar(任意关键字参数)`, 应该详细列出`*foo`和`**bar`。  
**Returns/Yields**:  
描述返回值的类型和语义。 如果函数返回 None, 这一部分可以省略。
**Raises**:  
列出与接口有关的所有异常。

```PYTHON
def fetch_bigtable_rows(big_table, keys, other_silly_variable=None):
    """Fetches rows from a Bigtable.

    Retrieves rows pertaining to the given keys from the Table instance
    represented by big_table.  Silly things may happen if
    other_silly_variable is not None.

    Args:
        big_table: An open Bigtable Table instance.
        keys: A sequence of strings representing the key of each table row.
        other_silly_variable: nothing.

    Returns:
        A dict mapping keys to the corresponding table row data
        fetched. Each row is represented as a tuple of strings. For
        example:

        {'Serak': ('Rigel VII', 'Preparer'),
         'Zim': ('Irk', 'Invader'),
         'Lrrr': ('Omicron Persei 8', 'Emperor')}

        If a key from the keys argument is missing from the dictionary,
        then that row was not found in the table.

    Raises:
        IOError: An error occurred accessing the bigtable.Table object.
    """
    pass
```

-----------------------------------------------------

### 2.2 类的注释
类应该在其定义下有一个用于描述该类的文档字符串。 如果你的类有公共属性(Attributes),  
那么文档中应该有一个属性(Attributes)段。 并且应该遵守和函数参数相同的格式。

```PYTHON
class SampleClass(object):
    """Summary of class here.

    Longer class information....
    Longer class information....

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """

    def __init__(self, likes_spam=False):
        """Inits SampleClass with blah."""
        self.likes_spam = likes_spam
        self.eggs = 0

    def public_method(self):
        """Performs operation blah."""
```
-----------------------------------------------------

### 2.3 块注释和行注释
最需要写注释的是代码中那些技巧性的部分。对于复杂的操作, 应该在其操作开始前写上若干行注释。   
对于不是一目了然的代码, 应在其行尾添加注释。

```PYTHON
# We use a weighted dictionary search to find out where i is in
# the array.  We extrapolate position based on the largest num
# in the array and the array size and then do binary search to
# get the exact number.

if i & (i-1) == 0:        # true if i is a power of 2
```

-----------------------------------------------------

### 2.4 注释文件生成
通过`pydoc`模块直接从代码中生成注释文档。

```PYTHON
> python -m pydoc -w <model> model.html
```

-----------------------------------------------------
&nbsp;

## 3. Coding
代码设计规范。

-----------------------------------------------------

### 3.1 类
如果一个类不继承自其它类, 就显式的从object继承。 嵌套类也一样。  

```PYTHON
class SampleClass(object):
    pass

class OuterClass(object):

    class InnerClass(object):
        pass

class ChildClass(ParentClass):
    """Explicitly inherits from another class already."""
```
-----------------------------------------------------

### 3.2 字符串
避免在循环中用+和+=操作符来累加字符串。 由于字符串是不可变的, 这样做会创建不必要的临时对象,  
并且导致二次方而不是线性的运行时间。 作为替代方案, 可以将每个子串加入列表, 在循环结束后用 `.join` 连接列表。

```PYTHON
x = a + b
x = '%s, %s!' % (imperative, expletive)
x = '{}, {}!'.format(imperative, expletive)
x = 'name: %s; score: %d' % (name, n)
x = 'name: {}; score: {}'.format(name, n)

items = ['<table>']
for last_name, first_name in employee_list:
    items.append('<tr><td>%s, %s</td></tr>' % (last_name, first_name))
items.append('</table>')
employee_table = ''.join(items)
```

-----------------------------------------------------

### 3.3 文件和socket
在文件和sockets结束时, 显式的关闭它。  

推荐使用`with`语句以管理文件:
```PYTHON
with open("hello.txt") as hello_file:
    for line in hello_file:
        print line
```

对于不支持使用`with`语句的类似文件的对象, 使用`contextlib.closing()`:
```PYTHON
import contextlib

with contextlib.closing(urllib.urlopen("http://www.python.org/")) as front_page:
    for line in front_page:
        print line
```

-----------------------------------------------------

### 3.4 TODO
TODO注释应该在所有开头处包含"TODO"字符串, 紧跟着是用括号括起来相关信息（开发者联系方式等），  
然后是一个可选的冒号。 接着必须有一行注释, 解释要做什么。

```python
# TODO(kl@gmail.com): Use a "*" here for string repetition.
# TODO(Zeke) Change this to use relations.
```

-----------------------------------------------------
### 3.5 Naming
命名约定。 所谓"内部(Internal)"表示仅模块内可用, 或者, 在类内是保护或私有的。  

+ 用单下划线(_)开头表示模块变量或函数是protected的(使用import * from时不会包含)
+ 用双下划线(__)开头的实例变量或方法表示类内私有
+ 将相关的类和顶级函数放在同一个模块里 
+ 对类名使用大写字母开头的单词(如CapWords), 但是模块名应该用小写加下划线的方式(如lower_with_under.py)
-----------------------------------------------------



