'''Tests for pyswmms. '''


# %% Run SWMM simulation through pyswmm.
from pyswmm import Simulation


sim = Simulation("./data/Example3.inp",
"./result/Example3_rpt.inp",
"./result/Example3_out.out")

sim.execute()
sim.report()
sim.close()


# %% Run SWMM simulation through pyswmm.
from pyswmm import Simulation


with Simulation("./data/Example3.inp", 
"./result/Example3_rpt.inp", 
"./result/Example3_out.out") as sim:
    for ind, step in enumerate(sim):
        if ind%100 == 0:
            print("step {}".format(ind))
    print("Complete!")
print("Closed!")


# %% Run SWMM simulation through pyswmm.
from pyswmm import Simulation, Nodes, Links


with Simulation("./data/Example3.inp", 
"./result/Example3_rpt.inp", 
"./result/Example3_out.out") as sim:
    node0 = Nodes(sim)["KRO3001"]
    link0 = Links(sim)["KRO1002-KRO1003"]
    
    def init_conditions():
        print("This is init functions added.")
        node0.initial_depth = 0.1
        link0.initial_flow = 1.0
    
    sim.initial_conditions(init_conditions)
    sim.start()
    for node, link in zip(Nodes(sim), Links(sim)):
        print("Node({}): {}, Link({}): {}".format(
            node.nodeid, node.initial_depth,
            link.linkid, link.initial_flow))


# %% Run PYSWMM with added callback functions.
from pyswmm import Simulation, Nodes, Links


def init_condition():
    print("This is init conditions added.")


def before_start():
    print("Callback function before start().")


def before_step():
    print("Callback function before step().")


def before_end():
    print("Callback function before end().")


def after_step():
    print("Callback function after step().")


def after_end():
    print("Callback function after end().")


def after_close():
    print("Callback function after close().")

# only worked through this type of calling Simulation.
with Simulation("./data/Example3.inp", 
"./result/Example3_rpt.inp", 
"./result/Example3_out.out") as sim:
    sim.initial_conditions(init_condition)
    sim.add_before_start(before_start)
    sim.add_before_step(before_step)
    sim.add_after_step(after_step)
    sim.add_before_end(before_end)
    sim.add_after_end(after_end)
    sim.add_after_close(after_close)
    sim.start()

    # sim.execute()  # would not activate after_end, before_end, after_close
    for ind, step in enumerate(sim):
        # if ind%200 == 0: print(step)
        pass

    sim.report


# %% Get Link object and its attributes.
from pyswmm import Simulation, Links


with Simulation("./data/Example3.inp", 
"./result/Example3_rpt.inp", 
"./result/Example3_out.out") as sim:
    for link in Links(sim):
        # print(link)
        # print(link.linkid, link.flow_limit)
        # print(link.linkid, link.target_setting)
        # print("Link({}) is conduit[True/False]: {}".format(link.linkid, link.is_conduit()))
        # if link.is_pump(): print(link.linkid)
        if link.is_orifice(): print(link.linkid, link.connections)
        # if link.is_outlet(): print(link.linkid)


# %%
