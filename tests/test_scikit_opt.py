# %%   kwargs

def display(**kwargs):
    if "setting" in kwargs:
        print("setting: {}".format(kwargs["setting"]))
    for key, value in kwargs.items():
        print("{}: {}".format(key, value), end="\t")
    print()

display(setting="Add", arg1=10, arg2=True)
args = {"setting":"Subtract", "arg1":-10, "arg2":False}
display(**args)


def show(name, age):
    print("name: {}, age: {}".format(name, age))

show("Tom", 12)
show(name="Mike", age=13)
show(age=15, name="Rose")



# %%  粒子群算法PSO(particle swarm optimization)
from sko.PSO import PSO
import matplotlib.pyplot as plt


def demo_func(x):
    x1, x2, x3 = x
    return x1**2 + (x2 - 0.05)**2 + x3**2


# PSO
# 入参	         默认值      意义
# func	         -	        目标函数
# n_dim	         -	        目标函数的维度
# size_pop	     50	        种群规模
# max_iter	     200	    最大迭代次数
# lb	         None	    每个参数的最小值
# ub	         None	    每个参数的最大值 
# w	             0.8	    惯性权重
# c1	         0.5	    个体记忆
# c2	         0.5	    集体记忆
pso = PSO(func=demo_func, dim=3, pop=40, max_iter=150, lb=[0, -1, 0.5],
           ub=[1, 1, 1], w=0.8, c1=0.5, c2=0.5)
pso.run()
print("best_x is {}, best_y is {}".format(pso.gbest_x, pso.best_y))


plt.plot(pso.gbest_y_hist)
plt.show()


# %%  遗传算法GA(Genetic Algorithm)
import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
from sko.GA import GA


def schaff(p):
    x1, x2 = p
    x = np.square(x1) + np.square(x2)
    return 0.5 + (np.square(np.sin(x)) - 0.5)/np.square(1 + 0.001*x)


# GA
# 入参	            默认值  	意义
# func	            -	       目标函数
# n_dim	            -	       目标函数的维度
# size_pop	        50	       种群规模
# max_iter	        200	       最大迭代次数
# prob_mut	        0.001	   变异概率
# lb	            -1	       每个参数的最小值
# ub	            1	       每个参数的最大值
# constraint_eq     空元组	    线性约束
# constraint_ueq	空元组	    非线性约束
# precision	1e-7	精准度，    int/float或者它们组成的列表
ga = GA(func=schaff, n_dim=2, size_pop=50, max_iter=800, lb=[-1, -1], ub=[1, 1], precision=1e-7)
best_x, best_y = ga.run()
print("best_x: {}\n, best_y: {}".format(best_x, best_y))

y_history = pd.DataFrame(ga.all_history_Y)
fig, ax = plt.subplots(2, 1)
ax[0].plot(y_history.index, y_history.values, ".", color="red")
y_history.min(axis=1).cummin().plot(kind="line")
plt.show()



# %%  标准测试函数
import math
import numpy as np
import pandas as pd 
from sko.PSO import PSO
from sko.GA import GA
import matplotlib.pyplot as plt

def testFunc1(x):
    '''单峰测试函数, Sphere函数。
    '''
    return sum([xi**2 for xi in x])

xlb = [-100 for i in range(0, 30)]
xub = [100 for i in range(0, 30)]
pso = PSO(func=testFunc1, dim=30, pop=300, max_iter=1000, lb=xlb, ub=xub)
pso.run()
print("best_x: {}, best_y: {}".format(pso.gbest_x, pso.gbest_y))
plt.plot(pso.gbest_y_hist)
plt.show()


def testFunc2(x):
    '''多峰测试函数，Ackley函数。
    '''
    N = float(len(x))
    part1 = math.exp(-0.2*math.sqrt(sum([xi**2 for xi in x])/N))
    part2 = math.exp(sum([math.cos(2*math.pi*xi) for xi in x])/N)
    return 20 + math.e - 20*part1 - part2

xlb = [-32 for i in range(0, 30)]
xub = [32 for i in range(0, 30)]
pso = PSO(func=testFunc2, dim=30, pop=200, max_iter=1000, lb=xlb, ub=xub, c1=0.2, c2=1.0)
pso.run()
print("best_x: {}, best_y: {}".format(pso.gbest_x, pso.gbest_y))
plt.plot(pso.gbest_y_hist)
plt.show()


def testFunc3(x):
    '''多峰测试函数，Griewank函数。
    '''
    N = len(x)
    part1 = sum([xi**2 for xi in x])/4000.0
    part2 = math.cos(x[0])
    for i in range(1, N):
        part2 *= math.cos(x[i]/math.sqrt(i+1))
    return part1 - part2 + 1

xlb = [-600 for i in range(0, 30)]
xub = [600 for i in range(0, 30)]
pso = PSO(func=testFunc3, dim=30, pop=200, max_iter=1000, lb=xlb, ub=xub)
pso.run()
print("best_x: {}, best_y: {}".format(pso.gbest_x, pso.gbest_y))
plt.plot(pso.gbest_y_hist)
plt.show()



# %%
