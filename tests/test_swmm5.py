# -*- encoding: utf-8 -*-
# -------------------------------------------------------------
'''
@Copyright :  Copyright(C) 2020, KeepSoft. All rights reserved.
@License   :  LGPL-3.0

@File      :  test_swmm5.py
@Time      :  2020/09/29
@Author    :  Qin ZhaoYu 
@Version   :  1.0

@Desc      :  Test to call swmm dll functions by python.
'''
# -------------------------------------------------------------


import os, sys
import ctypes
import unittest
import logging, time


class TestSwmmAPI(unittest.TestCase):
    '''Swmm API Test. 
      
    To test swmm51013.dll(x64) api by python.

    Not worked.
    '''

    dllfile = "F:\\PYSWMM_DEV\\tests\\swmm5.dll"
    infile  = b"F:\\PYSWMM_DEV\\tests\\data\\Example3.inp"
    rptfile = b"F:\\PYSWMM_DEV\\tests\\result\\rpt3.inp"
    outfile = b"F:\\PYSWMM_DEV\\tests\\result\\out3.inp"
   
    @classmethod
    def setUpClass(cls):
        '''To set up preinit conditions once.
           This would be run just once and run automatically by unittest.main().
        '''
        cls.infile  = ctypes.c_char_p(cls.infile)
        cls.rptfile = ctypes.c_char_p(cls.rptfile)
        cls.outfile = ctypes.c_char_p(cls.outfile)
        cls.__dll = ctypes.CDLL(cls.dllfile)
        print("setUpClass done.\n")

    def setUp(self):
        '''To do initialization befoe each test case run. '''
        self.__isEnded = False     # flag, to indicate wether swmm simulation endend.
        self.__isClosed = False    # flag, to indicate wether swmm simulation closed.
        print("setUp done.\n")

    def tearDown(self):
        '''To do postprocess after each test case run. '''
        if not self.__isEnded:
            self.__dll.swmm_end()
        if not self.__isClosed:
            self.__dll.swmm_close()
        print("\ntearDown done.")

    @classmethod
    def tearDownClass(cls):
        '''To destructure test class after all cases done. '''
        print("\ntearDownClass done.")

    def test_swmm_run(self):
        self.__dll.swmm_run.argtypes = [ctypes.c_char_p, ctypes.c_char_p, ctypes.c_char_p]
        self.__dll.swmm_run.restypes = ctypes.c_int

        error_code = self.__dll.swmm_run(self.infile, self.rptfile, self.outfile)
        self.__isEnded = True
        self.__isClosed = True
        self.assertEqual(error_code, 0)

    def test_swmm_by_steps(self):
        pass


if __name__ == "__main__":
    unittest.main()


