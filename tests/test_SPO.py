import sys
sys.path.append("F:\\")  # To add work dir to path.
import unittest
from pyswmm_dev.core.SluicePumpOperation import PumpOperation as po
from pyswmm_dev.core.SluicePumpOperation import SimMode
from pyswmm_dev.core.SluicePumpOperation import TargetFuncType


class TestPumpOperation(unittest.TestCase):
    '''To test class PumpOperation. 
    '''
    @classmethod
    def setUpClass(cls):
        cls.__po = po()
        cls.__files = {}
        cls.__files["inFile"] = "F:\\PYSWMM_DEV\\tests\\data\\Example3.inp"
        cls.__files["rptFile"] = "F:\\PYSWMM_DEV\\tests\\result\\rpt3.inp"
        cls.__files["outFile"] = "F:\\PYSWMM_DEV\\tests\\result\\out3.inp"

    @classmethod
    def tearDownClass(cls):
        del cls.__po

    def setUp(self):
        pass
 
    def tearDown(self):
        pass

    def testInit(self):
        statu = self.__po.init(inFile = "F:\\PYSWMM_DEV\\tests\\data\\Example3.inp")
        self.assertEqual(statu, True)
        statu = self.__po.init(**self.__files)
        self.assertEqual(statu, True)

    def testRun(self):
        self.__po.init(**self.__files)
        self.__po.setPSOArgs(pop=10, max_iter=10)        
        statu = self.__po.run(SimMode.SIM_SLUICE)
        self.assertEqual(statu, True)
        statu = self.__po.run(SimMode.SIM_SWMM)
        self.assertEqual(statu, True)
        target = self.__po.getTargetFuncVal()
        self.assertEqual(target, -1)
        statu = self.__po.run()
        self.assertEqual(statu, True)
        target = self.__po.getTargetFuncVal()
        self.assertEqual(target >= 0, True)
        statu = self.__po.run(SimMode.SIM_PUMP)
        self.assertEqual(statu, True)
    
    def testSetTargetFunc(self):
        # Subclass can't inherit default value of abstract method.
        statu = self.__po.setTargetFunc(TargetFuncType.TARGET_FLOOD)
        self.assertEqual(statu, True)
        statu = self.__po.setTargetFunc(8)
        self.assertEqual(statu, False)
        statu = self.__po.setTargetFunc(TargetFuncType.TARGET_CUSTOM)
        self.assertEqual(statu, False)
        statu = self.__po.setTargetFunc(TargetFuncType.TARGET_ADD)
        self.assertEqual(statu, False)
        def func(swmm):
            return 0
        statu = self.__po.setTargetFunc(TargetFuncType.TARGET_ADD, func)
        self.assertEqual(statu, True)
    
    def testSetPeriod(self):
        self.__po.init(**self.__files)
        start = [2001, 1, 1]
        end = [2001, 1, 2]
        statu = self.__po.setOperationPeriod(start, end)
        self.assertEqual(statu, True)
        end = [2001, 1, 2, 3]
        statu = self.__po.setOperationPeriod(start, end)
        self.assertEqual(statu, False)

    def testSetStep(self):
        # rpt step should be a multiple of route step.
        self.__po.init(**self.__files)
        statu = self.__po.setOperationStep(2)
        self.assertEqual(statu, True)
        statu = self.__po.setOperationStep(0.5)
        self.assertEqual(statu, True)

    def testSetPumps(self):
        pumpIds = ["PUMP1"]
        statu = self.__po.setOperationObjs(*pumpIds)
        self.assertEqual(statu, True)

        self.__po.init(**self.__files)
        pumpIds = ["PUMP1"]
        statu = self.__po.setOperationObjs(*pumpIds)
        self.assertEqual(statu, True)
        pumpIds = ["PUMP1", "PUMP2"]
        statu = self.__po.setOperationObjs(*pumpIds)
        self.assertEqual(statu, False)

    def testSetNodes(self):        
        self.__po.init(**self.__files)
        nodeIds = ["KRO3001", "KRO6015", "KRO1002", "NODE1"]
        statu = self.__po.setTargetObjs(*nodeIds)
        self.assertEqual(statu, False)        
        nodeIds = ["KRO3001", "KRO6015", "KRO1002"]
        statu = self.__po.setTargetObjs(*nodeIds)
        self.assertEqual(statu, True)

    def testSetPSO(self):
        self.__po.init(**self.__files)
        statu = self.__po.setPSOArgs(max_iter=10)
        self.assertEqual(statu, True)
        statu = self.__po.setPSOArgs(dim=50)
        self.assertEqual(statu, False)
        statu = self.__po.setPSOArgs(c3=5)
        self.assertEqual(statu, False)
        statu = self.__po.setPSOArgs(c1=0.5, c3=1.0)
        self.assertEqual(statu, False)

    def testGetSolution(self):
        self.__po.init(**self.__files)
        self.__po.setPSOArgs(pop=10, max_iter=10) 
        self.__po.run(SimMode.SIM_PUMP)
        solution = self.__po.getOperationSolution()
        print(solution)        
        self.assertEqual("pumps" in solution, True)
        self.assertEqual("PUMP1" in solution["pumps"], True)

    
    def testGetAllPumps(self):
        self.__po.init(**self.__files)
        ids = self.__po.getAllPumpIds()
        self.assertEqual("PUMP1" in ids, True)
        self.assertEqual("KRO2001-KRO2005" in ids, True)
        self.__po.setOperationObjs("KRO2001-KRO2005")
        ids = self.__po.getAllPumpIds()
        self.assertEqual("KRO2001-KRO2005" in ids, True)

    def testGetUsedPumps(self):
        self.__po.init(**self.__files)
        ids = self.__po.getAllOperationObjIds()
        self.assertEqual("PUMP1" in ids, True)
        self.__po.setOperationObjs("KRO2001-KRO2005")
        ids = self.__po.getAllOperationObjIds()
        self.assertEqual("PUMP1" in ids, False)

    def testGetAllSluices(self):
        self.__po.init(**self.__files)
        ids = self.__po.getAllSluiceIds()
        self.assertEqual("KRO1006-KRO1007" in ids, True)

    def testGetTargetValue(self):
        pass

    def testGetSimInfo(self):
        pass



if __name__ == "__main__":
    import os
    from pyswmm_dev.utils.Logger import MyLogger
    log = MyLogger("myLogger")
    log.setLogLevel(MyLogger.DEBUG)
    log.addFileHandler(logfile="./", level=MyLogger.INFO)

    caseDir = "F:\\PYSWMM_DEV\\tests\\"  # 测试用例保存目录
    testFile = "F:\\PYSWMM_DEV\\tests\\result\\testReport.txt"  # 测试结果报告文件
    discover = unittest.defaultTestLoader.discover(caseDir, "test_SPO.py")  # 加载用例
    with open(os.path.abspath(testFile), "w", encoding="utf8") as tf:
        runner = unittest.TextTestRunner(stream=tf, verbosity=2)
        runner.run(discover)
