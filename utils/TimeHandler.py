# -*- encoding: utf-8 -*-
# -------------------------------------------------------------
'''
@Copyright :  Copyright(C) 2020, KeepSoft. All rights reserved.  
@License   :  LGPL-3.0  

@File      :  TimeHandler.py  
@Time      :  2020/10/15  
@Author    :  Qin ZhaoYu   
@Version   :  1.0  

@Desc      :  To provid time handler for swmm dev.  
'''
# -------------------------------------------------------------


import datetime


class MyTimeHandler():
    '''To provid date time tools.
    '''
    def __init__(self):
        pass

    @staticmethod
    def turnDatetime(dateList) -> datetime.datetime:
        '''To turn date time list to datetime object.
          
        Args/Attributes:
        --------------------
        dateList : list of int. 
            List of datetime, formatted as (year month day hour minute second).
          
        Returns/Yeilds:
        --------------------
        A datetime object.
        '''
        return datetime.datetime(*dateList)

    @staticmethod
    def turnDatelist(dateTime) -> list:
        '''To turn datetime object to list.
          
        Args/Attributes:
        --------------------
        dateTime : list of int. 
            List of datetime, formatted as (year month day hour minute second).
          
        Returns/Yeilds:
        --------------------
        A datetime object.
        '''
        dateList = [dateTime.year, dateTime.month, dateTime.day, 
        dateTime.hour, dateTime.minute, dateTime.second]
        return dateList

    @staticmethod
    def getCurrentDate() -> datetime.datetime:
        '''To get current date time.          
          
        Returns/Yeilds:
        --------------------
        A datetime obj, Datetime.datetime(year month day hour minute second microseconds).
        '''
        return datetime.datetime.now()

    @staticmethod
    def getCurrentDateFormatted(fmt="%Y/%m/%d %H:%M:%S") -> str:
        '''To get current date time in specified format.
          
        Args/Attributes:
        --------------------
        fmt : str.  
            Date time output format. Default fmt = "%Y/%m/%d %H:%M:%S"
          
        Returns/Yeilds:
        --------------------
        A string of date time in specified format.
        '''
        time = datetime.datetime.now()
        return time.strftime(fmt)

    @staticmethod
    def getDateFormatted(date, fmt="%Y/%m/%d %H:%M:%S") -> str:
        '''To get current date time in specified format.
          
        Args/Attributes:
        --------------------
        date : list of int.  
            Input date time.
        fmt : str.  
            Date time output format. Default fmt = "%Y/%m/%d %H:%M:%S"
          
        Returns/Yeilds:
        --------------------
        A string of date time in specified format.
        '''
        return datetime.datetime(*date).strftime(fmt)
    
    @staticmethod
    def isEarlier(begin, end) -> bool:
        '''To check if begin time is no later then end time.
        
        In datetime objects, earlier date is smaller.

        Args/Attributes:
        --------------------
        begin : list of int.  
            The begin datetime, formated as "year month day hour minute second".
        end : list of int.
            The end datetime.

        Returns/Yeilds:
        --------------------
        True, if begin time is no later then end time; False, the others.
        '''
        return datetime.datetime(*begin) <= datetime.datetime(*end)

    @staticmethod
    def calcTimeInterval(begin, end, level="seconds") -> int:
        '''To calculate time interval between two time.
          
        Notice, the time interval under "seconds" level doesn't has sign.

        Args/Attributes:
        --------------------
        begin : list of int.  
            The begin datetime, formated as "year month day hour minute second".
        end : list of int.
            The end datetime.
        level : str.
            To specified calculate level. Default level = "seconds".
          
        Returns/Yeilds:
        --------------------
        Time interval under "days" or "seconds".
          
        Raises:
        --------------------
        ValueError: Error raises while invalid level.
        '''
        if level not in ["days", "seconds"]:
            raise ValueError("Invalid level({})".format(level))

        begin_time = datetime.datetime(*begin)
        end_time = datetime.datetime(*end)

        if level == "days":
            return (end_time - begin_time).days
        else:
            return (end_time - begin_time).seconds
        
    @staticmethod
    def calcDateTimeInterval(begin, end, level="seconds") -> int:
        '''To calculate time interval between two date time.
          
        Notice, the time interval under "seconds" level doesn't has sign. 

        Args/Attributes:
        --------------------
        begin : datetime.datetime.  
        end : datetime.datetime.
        level : str.
            To specified calculate level. Default level = "seconds".
          
        Returns/Yeilds:
        --------------------
        Time interval under "days" or "seconds".
          
        Raises:
        --------------------
        ValueError: Error raises while inputing invalid level.
        '''
        if level not in ["days", "seconds"]:
            raise ValueError("Invalid level({})".format(level))

        if level == "days":
            return (end - begin).days
        else:
            return (end - begin).seconds

    @staticmethod
    def updateTime(time, mode, **diff) -> list:
        '''To modify datetime with specified variations.
          
        Args/Attributes:
        --------------------
        time : list of int.  
            The datetime, formated as "year month day hour minute second".
        mode : str.
            The mode of modification, "add" or "subtract".
        diff : dict.  
            The variation, including "days", "hours", "minutes", "seconds".
          
        Returns/Yeilds:
        --------------------
        A date time list.
          
        Raises:
        --------------------
        ValueError: Error raises while invalid datetime or variation.
        '''
        if mode not in ["add", "subtract"]:
            raise ValueError("Invalid modification mode({})".format(mode))

        date = datetime.datetime(*time)
        if mode == "add":
            if "days" in diff:
                date = date + datetime.timedelta(days=diff["days"])
            if "hours" in diff:
                date = date + datetime.timedelta(hours=diff["hours"])
            if "minutes" in diff:
                date = date + datetime.timedelta(minutes=diff["minutes"])
            if "seconds" in diff:
                date = date + datetime.timedelta(seconds=diff["seconds"])
        else:
            if "days" in diff:
                date = date - datetime.timedelta(days=diff["days"])
            if "hours" in diff:
                date = date - datetime.timedelta(hours=diff["hours"])
            if "minutes" in diff:
                date = date - datetime.timedelta(minutes=diff["minutes"])
            if "seconds" in diff:
                date = date - datetime.timedelta(seconds=diff["seconds"])

        dateList = [date.year, date.month, date.day, date.hour, date.minute, date.second]
        return dateList

    @staticmethod
    def updateDateTime(time, mode, **diff) -> datetime.datetime:
        '''To modify datetime with specified variations.
          
        Args/Attributes:
        --------------------
        time : datetime.datetime.  
        mode : str.
            The mode of modification, "add" or "subtract".
        diff : dict.  
            The variation, including "days", "hours", "minutes", "seconds".
          
        Returns/Yeilds:
        --------------------
        A datetime.datetime.
          
        Raises:
        --------------------
        ValueError: Error raises while invalid datetime or variation.
        '''
        if mode not in ["add", "subtract"]:
            raise ValueError("Invalid modification mode({})".format(mode))

        date = time
        if mode == "add":
            if "days" in diff:
                date = date + datetime.timedelta(days=diff["days"])
            if "hours" in diff:
                date = date + datetime.timedelta(hours=diff["hours"])
            if "minutes" in diff:
                date = date + datetime.timedelta(minutes=diff["minutes"])
            if "seconds" in diff:
                date = date + datetime.timedelta(seconds=diff["seconds"])
        else:
            if "days" in diff:
                date = date - datetime.timedelta(days=diff["days"])
            if "hours" in diff:
                date = date - datetime.timedelta(hours=diff["hours"])
            if "minutes" in diff:
                date = date - datetime.timedelta(minutes=diff["minutes"])
            if "seconds" in diff:
                date = date - datetime.timedelta(seconds=diff["seconds"])
        return date


if __name__ == "__main__":
    tTool = MyTimeHandler()
    print("Do some testes")
    print("Current datetime: {}".format(tTool.getCurrentDate()))
    print("Current time: {}".format(tTool.getCurrentDateFormatted()))
    begin = datetime.datetime(2020, 10, 15)
    end = datetime.datetime(2020, 10, 16)
    print("Date day interval: {}\t{}\t{}".format("2020/10/15", "2020/10/16", 
    tTool.calcDateTimeInterval(begin, end, "days")))
    begin = [2020, 10, 15]
    end = [2020, 10, 16]
    print("Time second interval: {}\t{}\t{}".format("2020/10/15", "2020/10/16",
    tTool.calcTimeInterval(begin, end)))
    time = [2020, 10, 15]
    diff = {"days":1}
    print("Time add: {}\t{}\t{}".format("2020/10/15", "days=1", 
    tTool.updateTime(time, "add", **diff)))
    time = datetime.datetime(2020, 10, 15)
    print("Date subtract: {}\t{}\t{}".format("2020/10/15", "days=1", 
    tTool.updateDateTime(time, "subtract", **diff)))