# -*- encoding: utf-8 -*-
# -------------------------------------------------------------
'''
@Copyright :  Copyright(C) 2020, KeepSoft. All rights reserved.
@License   :  LGPL-3.0

@File      :  FileHandler.py
@Time      :  2020/10/09
@Author    :  Qin ZhaoYu 
@Version   :  1.0

@Desc      :  To provide file handler for swmm operation dev.
'''
# -------------------------------------------------------------


import os
import re
import ctypes


class MyStringHandler():
    '''To provide string tools. 
      s
    No guarantee that all methods had input checks.
    '''
    def __init__(self):
        pass

    @staticmethod
    def toUpper(*objStr):
        '''To turn str to upper.
        '''
        return [obj.upper for obj in objStr]

    @staticmethod
    def toLower(*objStr):
        '''To turn str to lower.
        '''
        return [obj.lower for obj in objStr]

    @staticmethod
    def turnBytes(*objStr) -> list:
        '''To turn str to bytes in utf8.
        '''
        res = []
        for s in objStr:
            res.append(s.encode())
        return res

    @staticmethod
    def turnStr(*objStr) -> list:        
        '''To turn bytes to str in utf8.
        '''
        res = []
        for s in objStr:
            res.append(s.decode())
        return res

    @staticmethod
    def turnCharPointer(*objStr) -> list:
        '''To turn str to c_char_p in ctypes.

        If a string is None or "", it would be replaced with a c string buffer.
        '''
        res = []
        for s in objStr:
            if not s:  # if s is None or "".
                res.append(ctypes.create_string_buffer(1024, '\0'))
            else:
                res.append(ctypes.c_char_p(s.encode()))
        return res
    
    @staticmethod
    def turnChar(*objStr) -> list:
        '''To turn str to c_char in ctypes.
        '''
        res = []
        for s in objStr:
            res.append(ctypes.c_char(s.encode()))
        return res

    @staticmethod
    def strBuffer(size):
        '''To get c char pointer with specified memory size.
        '''
        return ctypes.create_string_buffer(size, '\0')

    @staticmethod
    def isMatch(subStr, objStr):
        '''To check if subStr in objStr.
        '''
        if subStr in objStr:
            return True
        else:
            return False

    @staticmethod
    def splitStr(delims, objStr):
        '''To split str by delims.

        Args/Attributes:
        --------------------
        delims : str.    
            To provid delim characters, such as r"[;,\s]\s*".   
        objStr : str.  
            String needed splited.    

        Return/Yeild:
        --------------------
        A list contains splited results without delims.
        '''    
        return re.split(delims, objStr)

    @staticmethod
    def splitStrWithDelims(delims, objStr):
        '''To split str by delims.

        Args/Attributes:
        --------------------
        delims : str.    
            To provid delim characters, such as r"([;,\s])\s*".   
        objStr : str.  
            String needed splited. 

        Return/Yeild:
        --------------------
        A list contains splited results within delims.
        '''
        return re.split(delims, objStr)



class MyFileHandler():
    '''To provide file and folder tools. 

    No guarantee that all methods had input checks.
    '''

    def __init__(self):
        pass

    @staticmethod
    def turnAbsolute(path):
        '''To turn to absolutely path. 
        '''
        return os.path.abspath(path)

    @staticmethod
    def getCWD():
        '''To get current work direction.
        '''
        return os.getcwd()

    @staticmethod
    def isValidFilePath(path):
        '''To check if path is valid.
        '''
        parDir = os.path.pardir(path)
        if os.path.exists(path):
            return True
        else:
            return False

    @staticmethod
    def isExisted(path):
        '''To check if path existed.
        '''
        return os.path.exists(path)

    @staticmethod
    def isFile(path):
        '''To check if path points to a file.
        '''
        if os.path.exists(path) and os.path.isfile(path):
            return True
        else:
            return False

    @staticmethod
    def isFolder(path):
        '''To check if path points to a folder.
        '''
        if os.path.exists(path) and os.path.isdir(path):
            return True
        else:
            return False

    @staticmethod
    def splitFileName(path):
        '''To split file name. 
          
        Return/Yeild:
        --------------------
        A tuple constains file name and extension.
        '''
        return os.path.splitext(path)

    @staticmethod
    def getParentDir(path):
        '''To get parent direction of current path.
        '''
        return os.path.dirname(path)

    @staticmethod
    def joinPath(path, file):
        '''To join path and file.
        '''
        return os.path.join(path, file)
    
    # @staticmethod
    # def createFile(file):
    #     '''To open file by "a+" mode.
    #     '''
    #     with open(file, "a+") as f:
    #         return True
    #     return False

    @staticmethod
    def createFile(f, path=None):
        '''To create a file under specified dir.

        If "path" isn't being specified, then file would be created
        under current work direction.

        Return/Yeild:
        --------------------
        Path pointed to newly created file.
        '''
        if path:
            path = os.path.join(path, f)
        else:
            path = os.path.join(os.getcwd(), f)        
        with open(path, "w") as newf: pass
        return path

    @staticmethod
    def createFolder(dirs, path=None):
        '''To create a folder under specified dir.

        If "path" isn't being specified, then folder would be created
        under current work direction.

        Return/Yeild:
        --------------------
        Path pointed to newly created file.
        '''
        if path:
            path = os.path.join(path, dirs)
        else:
            path = os.path.join(os.getcwd(), dirs)
        os.makedirs(path)
        return path

    @staticmethod
    def searchCurrentLayerDir(path):
        '''To get all files and folders under specified current layer dir.

        If "path" isn't being specified, then search in current work direction.

        Return/Yeild:
        --------------------
        A list contains all files and folders in current layer dir.
        '''
        if path is None:
            path = os.getcwd()
        return os.listdir(path)

    @staticmethod
    def searchDir(path):
        '''To get all files and folders under specified dir.

        If "path" isn't being specified, then search in current work direction.
        This method would iterate over current dir and its subDirections.

        Return/Yeild:
        --------------------
        A tuple formated in (curDir, allFoldersCurDir, allFilesCurDir).
        '''
        if path is None:
            path = os.getcwd()
        return os.walk(path)

