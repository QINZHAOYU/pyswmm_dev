# -*- encoding: utf-8 -*-
# -------------------------------------------------------------
'''
@Copyright :  Copyright(C) 2020, KeepSoft. All rights reserved.
@License   :  LGPL-3.0

@File      :  Logger.py
@Time      :  2020/10/09
@Author    :  Qin ZhaoYu 
@Version   :  1.0

@Desc      :  To provide logger for swmm operation dev.
'''
# -------------------------------------------------------------


import os
import logging


class MyLogger():
    '''To provide simple logger function. 
    '''
    DEBUG = logging.DEBUG
    INFO = logging.INFO
    WARNING = logging.WARNING
    ERROR = logging.ERROR
    CRITICAL = logging.CRITICAL

    def __init__(self, logName):
        self.__logName = logName
        self.__log = logging.getLogger(self.__logName)
        self.__format = logging.Formatter(fmt="%(asctime)s, %(levelname)s, %(funcName)s, %(lineno)d, %(name)s: %(message)s")
        self.__logLevel = logging.WARNING
        self.__log.setLevel(self.__logLevel)

    def __isValidLevel(self, level):
        '''To check if "level" is valid. 
          
        Args/Attributes:
            level: logger, FileHandler or StreamHandler level.
          
        Returns/Yeilds:
            True, if "level" is valid; False, otherwise.
        '''
        if logging.CRITICAL < level or level < logging.NOTSET: 
            return False
        else:
            return True
    
    def getLogger(self):
        return self.__log

    def setLogLevel(self, logLevel):
        if self.__isValidLevel(logLevel): 
            self.__log.setLevel(logLevel)

    def addFileHandler(self, **kwargs):
        '''To generate and add FileHandler. 
          
        Args/Attributes:
            kwargs: A dict of FileHandler attributes, contains
                "level", "fmt", "logfile". If "logfile" not be specified,
                then would create a file named __logName under workDir.
          
        Returns/Yeilds:
            None.
          
        Raises:
            FileNotFoundError: An error while input "logfile" not existed.
        '''
        # set log file.
        if "logfile" in kwargs:
            logFile = kwargs["logfile"]
            if os.path.exists(logFile):
                if os.path.isdir(logFile):
                    logFile = os.path.join(logFile, self.__logName+".txt")
            else:
                raise FileNotFoundError("Log folder({}) not exits.".format(logFile))
        else:
            logFile = os.path.join(os.getcwd, self.__logName+".txt")
        
        # generate FileHandler.
        with open(logFile, "w") as f: pass
        fileHandler = logging.FileHandler(logFile, "a+", encoding="utf8")

        # set FileHandler level.
        if "level" in kwargs and self.__isValidLevel(kwargs["level"]):
            fileHandler.setLevel(kwargs["level"])
        else:
            fileHandler.setLevel(self.__logLevel)

        # set FileHandler format.
        if "fmt" in kwargs:
            fileHandler.setFormatter(kwargs["fmt"])
        else:
            fileHandler.setFormatter(self.__format)
        
        # add FileHandler to __log.
        self.__log.addHandler(fileHandler)

    def addStreamHandler(self, **kwargs):
        '''To generate and add StreamHandler. 
        '''
        streamHandler = logging.StreamHandler()
        # set StreamHandler level.
        if "level" in kwargs and self.__isValidLevel(kwargs["level"]):
            streamHandler.setLevel(kwargs["level"])
        else:
            streamHandler.setLevel(self.__logLevel)

        # set StreamHandler format.
        if "fmt" in kwargs:
            streamHandler.setFormatter(kwargs["fmt"])
        else:
            streamHandler.setFormatter(self.__format)
        
        # add StreamHandler to __log.
        self.__log.addHandler(streamHandler)
